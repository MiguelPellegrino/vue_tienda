import { createStore } from 'vuex'

export default createStore({
  state: {
    comida:[
      {producto: 'Manzana', precio:1.25, cantidad:0},
      {producto: 'Pera', precio:0.65, cantidad:0},
      {producto: 'Papa', precio:3, cantidad:0},
      {producto: 'Tomate', precio:0.52, cantidad:0},
      {producto: 'Coco', precio:3.52, cantidad:0},
      {producto: 'Sandia', precio:4, cantidad:0},
      {producto: 'Fresa', precio:1, cantidad:0},
      {producto: 'Cambur', precio:0.62, cantidad:0},
      {producto: 'Huevo', precio:15, cantidad:0},
      {producto: 'Pollo', precio:21, cantidad:0},
      {producto: 'Chile', precio:1, cantidad:0},
      {producto: 'Sardina', precio:1.5, cantidad:0},
    ]
  },
  mutations: {

  },
  actions: {
  },
  modules: {
  }
})
